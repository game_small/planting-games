import { _decorator, Component, Node } from 'cc';
const { ccclass, property } = _decorator;

@ccclass('Config')
export class Config {
    private environment = 'prod';

    public baseHost = '';

    public adoptUrl = '';

    public introduceUrl = '';

    public shareUrl = '';

    public shopUrl = '';
    public userUrl = '';

    constructor() {
        if (this.environment == 'test') {
            this.baseHost = 'http://m.tzshihu.com';
        } else if (this.environment == 'prod') {
            this.baseHost = 'https://ry.tzshihu.com';
        } else {
            this.baseHost = 'http://xhshihu.work';
        }

        this.adoptUrl = this.baseHost + '/index/index/mall.html';

        this.introduceUrl = this.baseHost + '/index/index/index.html';

        this.shareUrl = this.baseHost + '/index/index/poster.html';

        this.shopUrl = this.baseHost + '/index/index/mall.html';
        this.userUrl = this.baseHost + '/index/user/index.html';
    }
}

