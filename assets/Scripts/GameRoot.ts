import { _decorator, Component, Node } from 'cc';
const { ccclass, property } = _decorator;
import { UserGlobal } from './Globals/UserGlobal';

@ccclass('GameRoot')
export class GameRoot extends Component {
    onLoad() {
        let _user = UserGlobal.instance;
        // @ts-ignore
        _user.setUserId(COCOS_USERID);
        _user.getAssetNumber();
    }

    start() {

    }
}

