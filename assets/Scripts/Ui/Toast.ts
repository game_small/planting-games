import { _decorator, Component, Node, Label, UIOpacity, Color, tween, Vec3 } from 'cc';
const { ccclass, property } = _decorator;

@ccclass('Toast')
export class Toast extends Component {
    @property({ type: Label, displayName: '文本' })
    private label: Label = null;

    @property(UIOpacity)
    public UIOpacityBg: UIOpacity = null;

    public init(msg: string, code: number) {
        var self = this;
        this.label.string = msg;
        let color: Color = Color.WHITE;
        if (code == 0) {
            color = Color.GREEN;
        } else if (code == 1) {
            color = Color.RED;
        }
        this.label.color = color;

        setTimeout(() => {
            if (self.node) {
                tween(self.node)
                    .to(0.5, { position: new Vec3(0, 150, 0) }, { easing: 'smooth' })
                    .start();

                self.UIOpacityBg.opacity = 255;
                tween(self.UIOpacityBg)
                    .to(0.5, { opacity: 0 }, { easing: 'smooth' })
                    .call(() => {
                        self.node.removeFromParent();
                    }).start();
            }
        }, 2000);
    }
}

