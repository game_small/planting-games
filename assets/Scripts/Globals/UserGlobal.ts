import { _decorator, Component, Node } from 'cc';
const { ccclass, property } = _decorator;
import { HttpManager } from '../Utils/HttpManager';

@ccclass('UserGlobal')
export class UserGlobal {
    private static _instance: UserGlobal;

    static get instance() {
        if (this._instance) {
            return this._instance;
        }

        this._instance = new UserGlobal();
        this._instance.init();
        return this._instance;
    }

    private init() { }

    public _userId: number = 0;
    public setUserId(userId: number) {
        this._userId = userId;
    }

    public _landNumber: number = 0;
    public _sprinklerNumber: number = 0;
    public getAssetNumber() {
        if (this._userId <= 0) {
            return;
        }

        let url = '/game/api/asset';
        HttpManager.get(url, { user_id: this._userId }, (result) => {
            let data = result.data;
            this._landNumber = parseInt(data.adopt);
            this._sprinklerNumber = parseInt(data.sprinkler);
        });
    }
}

