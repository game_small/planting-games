import { _decorator, Component, Node, view } from 'cc';
const { ccclass, property } = _decorator;

@ccclass('Resize')
export class Resize extends Component {
    @property({ type: Node })
    private navNode: Node | null = null;
    @property({ type: Node })
    private quickNode: Node | null = null;
    @property({ type: Node })
    private gainNode: Node | null = null;

    onLoad() {
        this.adaptation(this);

        let self = this;
        view.setResizeCallback(function () {
            self.adaptation(self);
        });
    }

    adaptation(self: Resize) {
        let _size = view.getDesignResolutionSize();
        let _height = _size.height;
        let _view = view.getVisibleSize();
        let __height = _view.height;

        self.navNode.setPosition(0, (-399 * __height) / _height);

        self.quickNode.setPosition(0, (-263 * __height) / _height);

        // self.gainNode.setPosition(-60, (398 * __height) / _height);
    }
}

