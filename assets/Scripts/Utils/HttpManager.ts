import { _decorator, Component, Node } from 'cc';
const { ccclass, property } = _decorator;
import { Config } from '../Config';

// 超时请求
let timeout = 3000;

@ccclass('HttpManager')
export class HttpManager {
    public static get(url: string, data?: any, succCallback?: (response?: any) => void, errCallback?: (response?: any) => void) {
        if (data == null) {
            data = {}
        }

        let xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function () {
            if (xhr.readyState === 4 && xhr.status == 200) {
                let response = xhr.responseText;
                let rsp = JSON.parse(response);
                if (rsp.errcode == 0) {
                    succCallback && succCallback(rsp);
                    return;
                } else {
                    errCallback && errCallback(rsp);
                    return;
                }
            }
        };

        let config = new Config();
        let httpUrl = config.baseHost;
        httpUrl += url;
        httpUrl += '?' + this.queryParams(data);
        xhr.withCredentials = false;
        xhr.open('GET', httpUrl, true);
        xhr.timeout = timeout;

        xhr.ontimeout = function (e) {
            errCallback && errCallback({ errcode: 1, message: '请求超时' });
        }
        xhr.onerror = function (e) {
            errCallback && errCallback({ errcode: 1, message: '网络错误' });
        }
        xhr.send();
    }

    public static post(url: string, data?: any, succCallback?: (response?: any) => void, errCallback?: (response?: any) => void) {
        if (data == null) {
            data = {}
        }

        let xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function () {
            if (xhr.readyState === 4 && xhr.status == 200) {
                let response = xhr.responseText;
                let rsp = JSON.parse(response);
                if (rsp.errcode == 0) {
                    succCallback && succCallback(rsp);
                    return;
                } else {
                    errCallback && errCallback(rsp);
                    return;
                }
            }
        };

        let config = new Config();
        let httpUrl = config.baseHost;
        httpUrl += url;
        xhr.withCredentials = false;
        xhr.open('POST', httpUrl, true);
        xhr.setRequestHeader('Content-Type', 'application/json;charset=UTF-8');
        xhr.timeout = timeout;

        xhr.ontimeout = function (e) {
            errCallback && errCallback({ errcode: 1, message: '请求超时' });
        }
        xhr.onerror = function (e) {
            errCallback && errCallback({ errcode: 1, message: '网络错误' });
        }
        xhr.send(JSON.stringify(data));
    }

    private static queryParams(obj: any) {
        if (!obj) {
            return '';
        }
        let paramUrl = '';
        for (let key in obj) {
            paramUrl += key + '=' + obj[key] + '&';
        }
        if (paramUrl.length > 1) {
            paramUrl = paramUrl.substring(0, paramUrl.length - 1);
        }
        paramUrl = encodeURI(paramUrl);
        return paramUrl;
    }
}

