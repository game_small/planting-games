import { _decorator, Color, Component, Prefab, instantiate, Label, Layers, Material, Node, UIOpacity, Vec3, EventTouch } from 'cc';
const { ccclass, property } = _decorator;
import { HttpManager } from '../Utils/HttpManager';
import { UserGlobal } from '../Globals/UserGlobal';
import { UiManager } from './UiManager';

@ccclass('CoinManager')
export class CoinManager extends Component {
    @property({ type: Prefab })
    public droplet: Prefab | null = null;

    @property({ type: Label })
    public dailyLabel: Label | null = null;
    @property({ type: Label })
    public teamLabel: Label | null = null;
    @property({ type: Label })
    public inviteLabel: Label | null = null;


    start() {
        HttpManager.get('/game/api/coin', { user_id: UserGlobal.instance._userId }, (response) => {
            // let _gain = ('data' in response) ? response.data : [];
            // _gain = ('gain' in _gain) ? _gain.gain : [];
            // if ('daily' in _gain && 'team' in _gain && 'invite' in _gain) {
            //     this.updateGain(parseInt(_gain.daily), parseInt(_gain.team), parseInt(_gain.invite));
            // }

            let _coin = ('data' in response) ? response.data : [];
            _coin = ('coin' in _coin) ? _coin.coin : [];
            if (typeof (_coin) == 'object' && _coin.length > 0) {
                this.addCoin(_coin);
            }
        }, (response) => {
            let msg = ('message' in response) ? response.message : '获取待领取水滴错误';
            UiManager.showToast(msg, 1);
        });
    }

    private _scroll: boolean = false;

    updateGain(daily: number, team: number, invite: number) {
        if (daily > 0) {
            this._scroll = true;
            this.numberStart(this.dailyLabel, daily);
        }
        if (team > 0) {
            this._scroll = true;
            this.numberStart(this.teamLabel, team);
        }
        if (invite > 0) {
            this._scroll = true;
            this.numberStart(this.inviteLabel, invite);
        }
    }

    numberStart(label: Label, num: number) {
        let start = parseInt(label.string);
        num += start;
        let _base = 60;
        let _gap = num - start;
        if (_gap >= 200) {
            _base = 30;
        } else if (_gap >= 300) {
            _base = 15;
        }
        for (let i = 0; i <= _gap; i++) {
            setTimeout(() => {
                label.string = '' + (start + i);
                if (i == _gap) {
                    this._scroll = false;
                } else {
                    this._scroll = true;
                }
            }, _base * i);
        }
    }

    addCoin(coin: [object]) {
        for (let i = 0; i < coin.length; i++) {
            if (!('type' in coin[i]) || !('title' in coin[i]) ||
                !('amount' in coin[i]) || !('display' in coin[i])
            ) {
                continue;
            }

            let _length = this.node.children.length;
            let node = instantiate(this.droplet);
            let children = node.children;
            for (let j = 0; j < children.length; j++) {
                if (children[j].name == 'type') {
                    let _type = children[j].getComponent(Label);
                    _type.string = coin[i]['type'];
                } else if (children[j].name == 'title') {
                    let _title = children[j].getComponent(Label);
                    _title.string = coin[i]['title'];
                    children[j].active = true;
                } else if (children[j].name == 'amount') {
                    let _amount = children[j].getComponent(Label);
                    _amount.string = coin[i]['amount'];
                }
            }
            if (coin[i]['display'] == 'block') {
            } else {
                node.active = false;
            }
            node.on(Node.EventType.TOUCH_START, this.onCoinClicked, this);
            if (_length == 1) {
                node.setPosition(60, -10)
            } else if (_length == 2) {
                node.setPosition(-60, -10)
            }
            this.node.addChild(node);
        }
    }

    onCoinClicked(et: EventTouch) {
        if (this._scroll == true) {
            return;
        }

        let children = et.target.children;
        let _type, _amount;
        for (let i = 0; i < children.length; i++) {
            if (children[i].name == 'type') {
                let _label = children[i].getComponent(Label);
                _type = _label.string;
            } else if (children[i].name == 'amount') {
                let _label = children[i].getComponent(Label);
                _amount = parseInt(_label.string);
            }
        }

        let _from = et.target.getWorldPosition();
        let _to = null;
        if (_type == 'daily') {
            _to = this.dailyLabel.node.getWorldPosition();
        } else if (_type == 'team') {
            _to = this.teamLabel.node.getWorldPosition();
        } else if (_type == 'invite') {
            _to = this.inviteLabel.node.getWorldPosition();
        } else {
            return;
        }

        HttpManager.post('/game/api/coin', { type: _type, user_id: UserGlobal.instance._userId }, (response) => {
            console.log(response);
        }, (response) => {
            let msg = ('message' in response) ? response.message : '领取水滴出错';
            UiManager.showToast(msg, 1);
        });

        let _opacity = et.target.addComponent(UIOpacity);

        let _y = _to.y - _from.y;
        let _x = _to.x - _from.x;
        _x /= _y;
        let _o = 255 / _y;
        for (let i = 0; i <= _y; i++) {
            setTimeout(() => {
                et.target.setWorldPosition(new Vec3(_from.x + i * _x, _from.y + i));
                _opacity.opacity = (255 - _o * i);

                // if (i == _y) {
                //     if (_type == 'daily') {
                //         this.updateGain(_amount, 0, 0);
                //     } else if (_type == 'team') {
                //         this.updateGain(0, _amount, 0);
                //     } else if (_type == 'invite') {
                //         this.updateGain(0, 0, _amount);
                //     } else {
                //     }
                // }
            }, (i + 1) * 10);
        }
    }
}

