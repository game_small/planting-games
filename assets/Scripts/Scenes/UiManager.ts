import { _decorator, Component, Node, resources, Prefab, instantiate, find } from 'cc';
const { ccclass, property } = _decorator;

@ccclass('UiManager')
export class UiManager {
    public static showToast(msg: string, code: number = 2): void {
        resources.load('Prefabs/toast', Prefab, function (err, prefab: Prefab) {
            if (err) {
                console.log('载入预制资源失败,原因:' + err);
                return;
            }

            let tipPrefab: Node = instantiate(prefab);
            let tipScript = tipPrefab.getComponent('Toast');
            // @ts-ignore
            tipScript.init(msg, code);
            find('Canvas').addChild(tipPrefab);
        });
    }
}

