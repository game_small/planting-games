import { _decorator, Component, Prefab, instantiate, Node } from 'cc';
const { ccclass, property } = _decorator;
import { UserGlobal } from '../Globals/UserGlobal';

@ccclass('TreeManager')
export class TreeManager extends Component {
    @property({ type: Prefab })
    public treePrfb: Prefab | null = null;

    private lands: readonly Node[];

    start() {
        this.lands = this.node.parent.parent.children;
        this.treeInit();
    }

    treeInit() {
        if (UserGlobal.instance._landNumber <= 0) {
            return;
        }

        for (let i = 1; i <= UserGlobal.instance._landNumber; i++) {
            let _name = 'land' + i;

            setTimeout((name, index) => {
                for (let j = 0; j < this.lands.length; j++) {
                    if (this.lands[j].name == name) {
                        let node = instantiate(this.treePrfb);
                        this.lands[j].addChild(node);
                        node.setPosition(0, 0);
                    }
                }
            }, 100 * i, _name, i);
        }
    }
}

