import { _decorator, Component, Node } from 'cc';
const { ccclass, property } = _decorator;

@ccclass('LandManager')
export class LandManager extends Component {
    start() {
        let lands = this.node.children;
        for (let i = 0; i < lands.length; i++) {
            setTimeout(() => {
                this.showInit(lands[i]);
            }, 100 * (i + 1));
        }
    }

    showInit(land: Node) {
        let node = land.children;
        for (let i = 0; i < node.length; i++) {
            if (node[i].name == 'land') {
                node[i].active = true;
            }
        }
    }
}

