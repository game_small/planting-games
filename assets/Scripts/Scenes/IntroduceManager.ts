import { _decorator, Component, Node } from 'cc';
const { ccclass, property } = _decorator;
import { Config } from '../Config';

@ccclass('IntroduceManager')
export class IntroduceManager extends Component {
    start() {
        this.node.on(Node.EventType.TOUCH_START, this.onIntroductClicked);
    }

    onIntroductClicked() {
        let config = new Config();
        window.location.href = config.introduceUrl;
    }
}

