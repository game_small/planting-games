import { _decorator, Component, Label, Node, Prefab, instantiate, UITransform } from 'cc';
const { ccclass, property } = _decorator;
import { HttpManager } from '../Utils/HttpManager';
import { UserGlobal } from '../Globals/UserGlobal';

@ccclass('CertManager')
export class CertManager extends Component {
    @property({ type: Node })
    public closeNode: Node | null = null;
    @property({ type: Node })
    public contentNode: Node | null = null;
    @property({ type: Prefab })
    public certPrfb: Prefab | null = null;

    onLoad() {
        this.closeNode.on(Node.EventType.TOUCH_START, this.onCloseClicked, this);
    }

    start() {
        this.getCerts();
    }

    getCerts() {
        HttpManager.get('/game/api/cert', { user_id: UserGlobal.instance._userId }, (response) => {
            let data = ('data' in response) ? response.data : [];
            let _length = data.length;
            for (let i = 0; i < _length; i++) {
                let cert = instantiate(this.certPrfb);
                let _child = cert.children;
                for (let j = 0; j < _child.length; j++) {
                    let _name = _child[j].name;
                    let _label = _child[j].getComponent(Label);
                    if (_name == 'sn') {
                        _label.string = data[i]['order_sn'];
                    } else if (_name == 'desc') {
                        _label.string = '        在本游戏内，您于' + data[i]['create_date'] + '认养了1份\n\r石斛，期待您多使用洒水车，获得更好的游戏体验';
                    }
                }
                cert.setPosition(-136, -8 - i * 80);
                this.contentNode.addChild(cert);
            }

            let _contentUi = this.contentNode.getComponent(UITransform);
            _contentUi.height = _length * 80;
        });
    }


    onCloseClicked() {
        this.node.active = false;
    }
}

