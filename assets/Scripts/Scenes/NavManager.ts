import { _decorator, Component, Node } from 'cc';
const { ccclass, property } = _decorator;
import { Config } from '../Config';

@ccclass('NavManager')
export class NavManager extends Component {
    @property({ type: Node })
    private shopNode: Node | null = null;
    @property({ type: Node })
    private userNode: Node | null = null;

    start() {
        this.shopNode.on(Node.EventType.TOUCH_START, this.onShopClicked);
        this.userNode.on(Node.EventType.TOUCH_START, this.onUserClicked);
    }

    onShopClicked() {
        let config = new Config();
        window.location.href = config.shopUrl;
    }

    onUserClicked() {
        let config = new Config();
        window.location.href = config.userUrl;
    }
}

